﻿using DBDefsLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DBDefsLib.Structs;

namespace WDBXDefsGenerator.Defs
{
    /// <summary>
    /// The class for generating XML definitions for WDBX editor.
    /// </summary>
    public class DefsGenerator
    {
        public static DBDReader dbdReader = new DBDReader();

        public static string dbDefsDirectory;
        public static string outputName;
        public static string buildVersion;
        public int db2Processeds;

        public List<string> linesDefinitions = new List<string>();

        public DefsGenerator(string directory, string output, string build)
        {
            dbDefsDirectory = directory;
            outputName = output;
            buildVersion = build;
        }

        // Get the elementary type for the field (eg: string instead of locstring)
        /// <summary>
        /// Get the elementary type for the field (eg: string instead of locstring)
        /// </summary>
        /// <returns>
        /// An elementary type (string, float, ...)
        /// </returns>
        public string GetSimpleType(string type)
        {
            if (type.Contains("string"))
                type = "string";

            if (type.Contains("float"))
                type = "float";

            return type;
        }

        // If the size of a field is 0, we don't know the type so we need to fetch it manually in the column array and then sanitize it.
        /// <summary>
        /// If the size of a field is 0, we don't know the type so we need to fetch it manually in the column array and then sanitize it.
        /// </summary>
        /// <returns>
        /// The proper field of a column;
        /// </returns>
        public string GetUnknownColumnType(DBDefinition customDef, string columnName)
        {
            if (customDef.columnDefinitions.ContainsKey(columnName))
                return GetSimpleType(customDef.columnDefinitions[columnName].type);
            else
                return "unknown"; // should not happen !!!
        }

        //  Generate an XML definition with types and field names for a DB2
        /// <summary>
        /// Generate an XML definition with types and field names for a DB2
        /// </summary>
        /// <returns>
        /// A list of lines containing the full XML definition of the DB2;
        /// </returns>
        public List<string> GenerateDefinition(string db2Name, string buildString)
        {
            DBDReader dbdClass = new DBDReader();
            DBDefinition customDef = dbdClass.Read(dbDefsDirectory + "\\" + db2Name + ".dbd");
            Build build = new Build(buildString);

            List<string> definition = new List<string>();

            if (!DBDefsLib.Utils.GetVersionDefinitionByBuild(customDef, build, out VersionDefinitions? data))
                return definition;

            definition.Add($"  <Table Name=\"{db2Name}\" Build=\"{build.build}\">");

            var defArray = data.Value.definitions;

            for (int i = 0; i < defArray.Length; i++)
            {
                string fieldStringComplete = "";
                string fieldType = "";

                fieldStringComplete += $"    <Field Name=\"{defArray[i].name}\" ";

                switch (defArray[i].size)
                {
                    case 0:
                        fieldType = GetUnknownColumnType(customDef, defArray[i].name);
                        break;
                    case 8:
                        fieldType = "byte";
                        break;
                    case 16:
                        fieldType = "short";
                        break;
                    case 32:
                        fieldType = "int";
                        break;
                    case 64:
                        fieldType = "long";
                        break;
                }

                fieldStringComplete += $"Type=\"{fieldType}\" ";

                if (defArray[i].isID)
                    fieldStringComplete += $"IsIndex=\"true\"";

                if (defArray[i].arrLength > 0)
                    fieldStringComplete += $"ArraySize=\"{defArray[i].arrLength}\"";

                fieldStringComplete += $"/>";

                definition.Add(fieldStringComplete);

            }

            definition.Add("  </Table>");

            return definition;
        }


        //  Generate for all DB2(s) a proper definition
        /// <summary>
        /// Generate for all DB2(s) a proper definition
        /// </summary>
        /// </returns>
        public void Process()
        {
            string[] allDefinitions = Directory.GetFiles(@"definitions", "*.dbd");
            db2Processeds = allDefinitions.Length;

            linesDefinitions.Add("<?xml version=\"1.0\"?>");
            linesDefinitions.Add("<Definition xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");

            foreach (var def in allDefinitions)
            {
                linesDefinitions = linesDefinitions.Concat(GenerateDefinition(Path.GetFileNameWithoutExtension(def), buildVersion)).ToList();
            }


            linesDefinitions.Add("</Definition>");

            File.WriteAllLines(outputName, linesDefinitions);
        }



    }
}

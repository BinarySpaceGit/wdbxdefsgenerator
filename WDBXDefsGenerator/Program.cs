﻿using System;
using System.Diagnostics;
using System.IO;
using WDBXDefsGenerator.Defs;

namespace WDBXDefsGenerator
{
    class Program
    {
 
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Not enough arguments. WDBXDefsGenerator.exe {build} {output}");
                return;
            }

            Console.WriteLine("[+] Starting to generate definitions for builds");

            var watch = new Stopwatch();
            watch.Start();

            DefsGenerator generator = new DefsGenerator("definitions", args[1], args[0]);
            generator.Process();

            watch.Stop();
            Console.WriteLine($"[+] Finished to generate defs for {generator.db2Processeds} db2(s) in {watch.Elapsed}");

        }
    }
}
